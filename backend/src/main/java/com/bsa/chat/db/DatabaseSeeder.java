package com.bsa.chat.db;

import com.bsa.chat.role.model.Role;
import com.bsa.chat.role.model.UserRole;
import com.bsa.chat.role.repository.RoleRepository;
import com.bsa.chat.user.model.User;
import com.bsa.chat.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component
public class DatabaseSeeder {

    private final RoleRepository roleRepository;

    private final UserRepository userRepository;


    @Autowired
    public DatabaseSeeder(RoleRepository roleRepository, UserRepository userRepository) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    public void seed() {
        if (userRepository.count() > 0) {
            return;
        }
        final var roles = roleRepository.saveAll(List.of(new Role(UserRole.ROLE_USER), new Role(UserRole.ROLE_ADMIN)));
        final var user = User.builder()
                .username("Ruth User1")
                .email("test@test.test")
                .password("$2y$12$klHv0pL1/6iAo2yucpF74enZvE1J9Ajp.HXAzVxQev05mh3ECNov6") //test
                .avatarUrl("https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA")
                .roles(Set.of(roles.get(0)))
                .build();
        final var admin = User.builder()
                .username("Wendy Admin1")
                .email("admin@test.test")
                .password("$2y$12$8WoNKtlMe.cE8cXQOILycuCMbyJVqzYf4bCd8nduhf8z4Y9dw7NES") //admin
                .avatarUrl("https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng")
                .roles(Set.of(roles.get(1)))
                .build();
        userRepository.saveAll(List.of(user, admin));
    }
}
