package com.bsa.chat.configuration;

public class SecurityConstants {

    protected static final String[] WHITE_ROUTES_LIST = {"/api/auth/**", "/api/messages"};

    private SecurityConstants() {
    }

}
