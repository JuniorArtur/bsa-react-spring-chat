package com.bsa.chat.role.model;

public enum UserRole {
    ROLE_USER,
    ROLE_ADMIN
}
