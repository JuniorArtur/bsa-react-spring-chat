package com.bsa.chat.role.dto;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
public class RoleDto {

    private final UUID id;

    private final String name;

}
