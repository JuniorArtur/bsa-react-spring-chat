package com.bsa.chat.message.dto;

import com.bsa.chat.message.model.Message;

import java.util.List;
import java.util.stream.Collectors;

public interface MessageDtoMapper {

    static Message mapMessageDtoToMessage(MessageDto message) {
        return Message.builder()
                .id(message.getId())
                .text(message.getText())
                .author(message.getAuthor())
                .createdAt(message.getCreatedAt())
                .updatedAt(message.getUpdatedAt())
                .build();
    }

    static List<Message> mapMessageDtosToMessages(List<MessageDto> messages) {
        return messages
                .stream()
                .map(MessageDtoMapper::mapMessageDtoToMessage)
                .collect(Collectors.toUnmodifiableList());
    }

}
