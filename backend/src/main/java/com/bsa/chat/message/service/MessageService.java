package com.bsa.chat.message.service;

import com.bsa.chat.message.dto.MessageDto;
import com.bsa.chat.message.dto.MessageDtoMapper;
import com.bsa.chat.message.model.Message;
import com.bsa.chat.message.model.MessageMapper;
import com.bsa.chat.message.repository.MessageRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class MessageService {

    private final MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public List<MessageDto> findAll() {
        return MessageMapper.mapMessagesToMessageDtos(
                this.messageRepository.findAll(Sort.by(Sort.Direction.ASC, "createdAt"))
        );
    }

    public MessageDto findById(UUID messageId) throws NotFoundException {
        return MessageMapper.mapMessageToMessageDto(
                this.messageRepository.findById(messageId).orElseThrow(
                        () -> new NotFoundException("Message with id '" + messageId.toString() + "' is not found.")
                )
        );
    }

    public MessageDto save(MessageDto messageDto) {
        final var message = MessageDtoMapper.mapMessageDtoToMessage(messageDto);
        return MessageMapper.mapMessageToMessageDto(this.messageRepository.save(message));
    }

    public MessageDto updateById(UUID messageId, Message message) throws NotFoundException {
        if (!this.messageRepository.existsById(messageId)) {
            throw new NotFoundException("message with id '" + messageId.toString() + "' is not found.");
        }
        message.setId(messageId);
        return MessageMapper.mapMessageToMessageDto(this.messageRepository.save(message));
    }

    public void deleteById(UUID messageId) {
        this.messageRepository.deleteById(messageId);
    }

}
