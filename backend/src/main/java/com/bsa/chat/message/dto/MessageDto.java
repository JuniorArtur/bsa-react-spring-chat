package com.bsa.chat.message.dto;


import com.bsa.chat.user.model.User;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Data
public class MessageDto {

    private final UUID id;

    private final String text;

    private final User author;

    private final LocalDateTime createdAt;

    private final LocalDateTime updatedAt;

}
