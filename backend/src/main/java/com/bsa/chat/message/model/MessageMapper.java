package com.bsa.chat.message.model;

import com.bsa.chat.message.dto.MessageDto;

import java.util.List;
import java.util.stream.Collectors;

public interface MessageMapper {

    static MessageDto mapMessageToMessageDto(Message message) {
        return MessageDto.builder()
                .id(message.getId())
                .text(message.getText())
                .author(message.getAuthor())
                .createdAt(message.getCreatedAt())
                .updatedAt(message.getUpdatedAt())
                .build();
    }

    static List<MessageDto> mapMessagesToMessageDtos(List<Message> messages) {
        return messages
                .stream()
                .map(MessageMapper::mapMessageToMessageDto)
                .collect(Collectors.toUnmodifiableList());
    }

}
