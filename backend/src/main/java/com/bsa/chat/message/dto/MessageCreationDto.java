package com.bsa.chat.message.dto;

import com.bsa.chat.user.model.User;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Builder
@Data
public class MessageCreationDto {

    private final String text;

    private final User author;

    private final LocalDateTime createdAt;

}
