package com.bsa.chat.message.controller;

import com.bsa.chat.message.service.MessageService;
import com.bsa.chat.message.dto.MessageDto;
import com.bsa.chat.message.dto.MessageDtoMapper;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("/api/messages")
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    public ResponseEntity<List<MessageDto>> getAllMessages() {
        return ResponseEntity.ok(this.messageService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<MessageDto> getMessageById(@PathVariable("id") UUID messageId) {
        try {
            return ResponseEntity.ok(this.messageService.findById(messageId));
        } catch (NotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage(), ex);
        }
    }

    @PostMapping
    public ResponseEntity<MessageDto> saveMessage(MessageDto message) {
        final var createdReaction = this.messageService.save(message);
        final var location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdReaction.getId())
                .toUri();
        return ResponseEntity
                .created(location)
                .body(createdReaction);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MessageDto> updateMessage(@PathVariable("id") UUID messageId, MessageDto message) {
        try {
            final var updatedMessage = this.messageService.updateById(
                    messageId, MessageDtoMapper.mapMessageDtoToMessage(message)
            );
            return ResponseEntity.ok(updatedMessage);
        } catch (NotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage(), ex);
        }
    }

    @DeleteMapping
    public ResponseEntity<HttpStatus> deleteMessageById(UUID messageId) {
        this.messageService.deleteById(messageId);
        return ResponseEntity
                .noContent()
                .build();
    }

}
