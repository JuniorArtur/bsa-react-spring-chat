package com.bsa.chat.auth.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@AllArgsConstructor
@Data
public class AuthDto {

    private UUID userId;

    private String role;

    private Boolean isAuthorized;

}
