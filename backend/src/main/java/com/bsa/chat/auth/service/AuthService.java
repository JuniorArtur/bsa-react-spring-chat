package com.bsa.chat.auth.service;

import com.bsa.chat.ChatApplication;
import com.bsa.chat.auth.dto.LoginResponseDto;
import com.bsa.chat.auth.dto.UserDetailsDto;
import com.bsa.chat.auth.dto.LoginRequestDto;
import com.bsa.chat.user.dto.UserDto;
import com.bsa.chat.user.service.UserService;
import javassist.NotFoundException;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class AuthService {

    private static final Logger logger = LoggerFactory.logger(ChatApplication.class);

    private final DaoAuthenticationProvider authenticationProvider;

    private final UserService userDetailsService;

    private UserDto currentUser;

    @Autowired
    public AuthService(DaoAuthenticationProvider authenticationProvider, UserService userDetailsService) {
        this.authenticationProvider = authenticationProvider;
        this.userDetailsService = userDetailsService;
        try {
            this.currentUser = userDetailsService.findByEmail("test@test.test");
        } catch (NotFoundException e) {
        }
    }

    public LoginResponseDto authenticate(LoginRequestDto user) throws NotFoundException {
        final var auth = authenticationProvider.authenticate(
                new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(auth);
        logger.info("Authentication result: " + auth.toString());
        final var userDetails = (UserDetailsDto)auth.getPrincipal();
        final var existingUser = userDetailsService.findByEmail(userDetails.getEmail());
        return LoginResponseDto.from(existingUser);
    }

    public void logout() {
        this.currentUser = null;
    }

    public UUID getCurrentUserId() {
        return this.currentUser.getId();
    }
}
