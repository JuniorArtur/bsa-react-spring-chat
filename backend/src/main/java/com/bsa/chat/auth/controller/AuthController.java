package com.bsa.chat.auth.controller;

import com.bsa.chat.ChatApplication;
import com.bsa.chat.auth.dto.AuthDto;
import com.bsa.chat.auth.dto.LoginRequestDto;
import com.bsa.chat.auth.dto.LoginResponseDto;
import com.bsa.chat.auth.service.AuthService;
import javassist.NotFoundException;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private static final Logger logger = LoggerFactory.logger(ChatApplication.class);

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping("/current")
    public UUID getCurrentUserId(){
        return authService.getCurrentUserId();
    }

    @GetMapping
    public AuthDto getAuthorization() {
        return new AuthDto(UUID.randomUUID(), "ADMIN", true);
    }

    @PutMapping("/logout")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void logout() {
        authService.logout();
    }

    @PostMapping("/signin")
    public ResponseEntity<LoginResponseDto> login(@RequestBody LoginRequestDto user) throws NotFoundException {
        logger.info("Login request: " + user.toString());
        return ResponseEntity.ok(authService.authenticate(user));
    }

}
