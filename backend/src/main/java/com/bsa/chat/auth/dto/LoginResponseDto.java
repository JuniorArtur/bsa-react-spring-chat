package com.bsa.chat.auth.dto;

import com.bsa.chat.user.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public class LoginResponseDto {

    private final UUID id;

    private final String username;

    private final String email;

    private final Collection<String> roles;

    public static LoginResponseDto from(UserDto user) {
        final var authorities = user.getRoles().stream()
                .map(role -> role.getName().name())
                .collect(Collectors.toUnmodifiableList());

        return new LoginResponseDto(
                user.getId(),
                user.getUsername(),
                user.getEmail(),
                authorities
        );
    }

}
