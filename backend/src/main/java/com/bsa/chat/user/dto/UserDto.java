package com.bsa.chat.user.dto;

import com.bsa.chat.role.model.Role;
import lombok.*;

import java.util.Set;
import java.util.UUID;

@Builder
@Getter
public class UserDto {

    private final UUID id;

    private final String email;

    private final String password;

    private final String username;

    private final String avatarUrl;

    private final Set<Role> roles;

}
