package com.bsa.chat.user.controller;

import com.bsa.chat.user.dto.UserCreationDto;
import com.bsa.chat.user.dto.UserDto;
import com.bsa.chat.user.dto.UserDtoMapper;
import com.bsa.chat.user.service.UserService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("/api/users")
@PreAuthorize("hasRole('ADMIN')")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<List<UserDto>> getAllUsers() {
        return ResponseEntity.ok(this.userService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable("id") UUID userId) {
        try {
            return ResponseEntity.ok(this.userService.findById(userId));
        } catch (NotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage(), ex);
        }
    }

    @PostMapping
    public ResponseEntity<UserDto> saveUser(UserCreationDto user) {
        final var createdUser = this.userService.save(user);
        final var location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdUser.getId())
                .toUri();
        return ResponseEntity
                .created(location)
                .body(createdUser);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDto> updateUser(@PathVariable("id") UUID userId, UserDto user) {
        try {
            final var updatedUser = this.userService.updateById(userId, UserDtoMapper.mapUserDtoToUser(user));
            return ResponseEntity.ok(updatedUser);
        } catch (NotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage(), ex);
        }
    }

    @DeleteMapping
    public ResponseEntity<HttpStatus> deleteUserById(UUID userId) {
        this.userService.deleteById(userId);
        return ResponseEntity
                .noContent()
                .build();
    }

}
