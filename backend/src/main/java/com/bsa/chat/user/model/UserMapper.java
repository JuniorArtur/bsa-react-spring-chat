package com.bsa.chat.user.model;

import com.bsa.chat.user.dto.UserDto;

import java.util.List;
import java.util.stream.Collectors;

public interface UserMapper {

    static UserDto mapUserToUserDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .email(user.getEmail())
                .password(user.getPassword())
                .username(user.getUsername())
                .avatarUrl(user.getAvatarUrl())
                .roles(user.getRoles())
                .build();
    }

    static List<UserDto> mapUsersToUserDtos(List<User> users) {
        return users.stream()
                .map(UserMapper::mapUserToUserDto)
                .collect(Collectors.toUnmodifiableList());
    }

}
