package com.bsa.chat.user.dto;

import com.bsa.chat.user.model.User;

import java.util.List;
import java.util.stream.Collectors;

public interface UserCreationDtoMapper {

    static User mapUserCreationDtoToUser(UserCreationDto user) {
        return User.builder()
                .email(user.getEmail())
                .password(user.getPassword())
                .username(user.getUsername())
                .build();
    }

    static List<User> mapUserCreationDtosToUsers(List<UserCreationDto> users) {
        return users.stream()
                .map(UserCreationDtoMapper::mapUserCreationDtoToUser)
                .collect(Collectors.toUnmodifiableList());
    }

}
