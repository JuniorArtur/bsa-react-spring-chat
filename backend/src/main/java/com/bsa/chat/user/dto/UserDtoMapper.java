package com.bsa.chat.user.dto;

import com.bsa.chat.user.model.User;

import java.util.List;
import java.util.stream.Collectors;

public interface UserDtoMapper {

    static User mapUserDtoToUser(UserDto user) {
        return User.builder()
                .id(user.getId())
                .email(user.getEmail())
                .password(user.getPassword())
                .username(user.getUsername())
                .avatarUrl(user.getAvatarUrl())
                .roles(user.getRoles())
                .build();
    }

    static List<User> mapUserDtosToUsers(List<UserDto> userDtos) {
        return userDtos.stream()
                .map(UserDtoMapper::mapUserDtoToUser)
                .collect(Collectors.toUnmodifiableList());
    }

}
