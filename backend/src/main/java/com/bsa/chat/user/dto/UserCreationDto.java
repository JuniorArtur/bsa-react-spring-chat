package com.bsa.chat.user.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class UserCreationDto {

    private final String username;

    private final String email;

    private String password;

}
