package com.bsa.chat.user.service;

import com.bsa.chat.auth.dto.UserDetailsDto;
import com.bsa.chat.role.model.Role;
import com.bsa.chat.role.model.UserRole;
import com.bsa.chat.role.repository.RoleRepository;
import com.bsa.chat.user.dto.UserCreationDto;
import com.bsa.chat.user.dto.UserCreationDtoMapper;
import com.bsa.chat.user.dto.UserDto;
import com.bsa.chat.user.model.User;
import com.bsa.chat.user.model.UserMapper;
import com.bsa.chat.user.repository.UserRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class UserService implements UserDetailsService {

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = new BCryptPasswordEncoder();
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) {
        final var user = this.userRepository.findByEmail(username);
        return UserDetailsDto.from(user.orElseThrow(() -> new UsernameNotFoundException(username)));
    }

    public List<UserDto> findAll() {
        return UserMapper.mapUsersToUserDtos(this.userRepository.findAll());
    }

    public UserDto findById(UUID userId) throws NotFoundException {
        return UserMapper.mapUserToUserDto(
                this.userRepository.findById(userId).orElseThrow(
                        () -> new NotFoundException("User id: " + userId.toString())
                )
        );
    }

    public UserDto findByEmail(String email) throws NotFoundException {
        return UserMapper.mapUserToUserDto(
                this.userRepository.findByEmail(email).orElseThrow(
                        () -> new NotFoundException("User email: " + email)
                )
        );
    }

    public UserDto save(UserCreationDto userCreationDto) {
        userCreationDto.setPassword(this.passwordEncoder.encode(userCreationDto.getPassword()));
        final var user = UserCreationDtoMapper.mapUserCreationDtoToUser(userCreationDto);
        user.setRoles(Set.of(new Role(UserRole.ROLE_USER)));
        return UserMapper.mapUserToUserDto(this.userRepository.save(user));
    }

    public UserDto updateById(UUID userId, User user) throws NotFoundException {
        if (!this.userRepository.existsById(userId)) {
            throw new NotFoundException("User id: " + userId.toString());
        }
        user.setId(userId);
        return UserMapper.mapUserToUserDto(this.userRepository.save(user));
    }

    public void deleteById(UUID userId) {
        this.userRepository.deleteById(userId);
    }

}
