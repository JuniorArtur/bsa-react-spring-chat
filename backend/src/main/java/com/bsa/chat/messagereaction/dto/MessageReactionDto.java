package com.bsa.chat.messagereaction.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class MessageReactionDto {

    private UUID id;

    private Boolean isLiked;

}
