package com.bsa.chat.messagereaction.service;

import com.bsa.chat.message.repository.MessageRepository;
import com.bsa.chat.messagereaction.dto.MessageReactionCreationDto;
import com.bsa.chat.messagereaction.dto.MessageReactionDto;
import com.bsa.chat.messagereaction.model.MessageReaction;
import com.bsa.chat.messagereaction.repository.MessageReactionRepository;
import com.bsa.chat.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageReactionService {

    private final MessageReactionRepository reactionRepository;

    private final UserRepository userRepository;

    private final MessageRepository messageRepository;

    @Autowired
    public MessageReactionService(MessageReactionRepository reactionRepository, UserRepository userRepository,
                                  MessageRepository messageRepository) {
        this.reactionRepository = reactionRepository;
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
    }

    public MessageReactionDto saveMessageReaction(MessageReactionCreationDto reaction) {
        final var prevReaction = reactionRepository.findByUserIdAndMessageId(
                reaction.getUserId(), reaction.getMessageId()
        );
        if (prevReaction.isEmpty()) {
            final var newReaction = new MessageReaction();
            newReaction.setMessage(messageRepository.findById(reaction.getMessageId()).orElseThrow());
            newReaction.setUser(userRepository.findById(reaction.getUserId()).orElseThrow());
            final var savedReaction = reactionRepository.save(newReaction);
            return new MessageReactionDto(savedReaction.getId(), savedReaction.getIsLiked());
        }
        final var updatedReaction = prevReaction.get();
        updatedReaction.setIsLiked(!updatedReaction.getIsLiked());
        reactionRepository.save(updatedReaction);
        return new MessageReactionDto(updatedReaction.getId(), updatedReaction.getIsLiked());
    }

}
