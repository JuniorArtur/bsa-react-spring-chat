package com.bsa.chat.messagereaction.controller;

import com.bsa.chat.messagereaction.dto.MessageReactionDto;
import com.bsa.chat.messagereaction.dto.MessageReactionCreationDto;
import com.bsa.chat.messagereaction.service.MessageReactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@CrossOrigin
@RestController
@RequestMapping("/api/reactions")
@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
public class MessageReactionController {

    private final MessageReactionService messageReactionService;

    @Autowired
    public MessageReactionController(MessageReactionService messageReactionService) {
        this.messageReactionService = messageReactionService;
    }

    @PostMapping
    public ResponseEntity<MessageReactionDto> saveReaction(@RequestBody MessageReactionCreationDto messageReaction) {
        final var createdReaction = messageReactionService.saveMessageReaction(messageReaction);
        final var location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdReaction.getId())
                .toUri();
        return ResponseEntity
                .created(location)
                .body(createdReaction);
    }

}
