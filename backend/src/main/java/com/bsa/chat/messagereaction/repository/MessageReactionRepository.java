package com.bsa.chat.messagereaction.repository;

import com.bsa.chat.messagereaction.model.MessageReaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MessageReactionRepository extends JpaRepository<MessageReaction, UUID> {

    Optional<MessageReaction> findByUserIdAndMessageId(UUID userId, UUID messageId);

}
