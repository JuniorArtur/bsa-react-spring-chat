package com.bsa.chat.messagereaction.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class MessageReactionCreationDto {

    private UUID userId;

    private UUID messageId;

}
