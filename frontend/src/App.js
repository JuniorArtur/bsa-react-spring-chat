import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import Chat from "./containers/Chat/chat";
import LoginPage from "./containers/LoginPage";
import UserList from "./components/UserList";
import UserEditor from "./components/UserEditor";
import MessageEditor from "./components/MessageEditor";

const App = () => (
  <Switch>
      <Route exact path="/" component={Chat}/>
      <Route exact path="/login" component={LoginPage}/>
      <Route exact path="/users" component={UserList}/>
      <Route path="/message/:id" component={MessageEditor}/>
      <Route exact path="/user" component={UserEditor}/>
      <Route path="/user/:id" component={UserEditor}/>
      <Redirect to="/"/>
  </Switch>
);

export default App;
