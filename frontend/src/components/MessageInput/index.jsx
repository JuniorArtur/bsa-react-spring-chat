import React, {useState} from 'react';
import {Button, Icon, TextArea} from 'semantic-ui-react';
import './styles.css';

export const MessageInput = ({addMessage: sendMessage, userName, avatar}) => {
  const [messageBody, setMessageBody] = useState('');

  const onSubmit = e => {
    e.preventDefault();
    if (messageBody.trim().length > 0) {
      sendMessage(messageBody, userName, avatar);
      setMessageBody('');
    }
  };

  return (
    <form className='field message-input' onSubmit={onSubmit}>
      <TextArea
        placeholder='Message'
        value={messageBody}
        className='message-input__field'
        onChange={e => setMessageBody(e.target.value)}
      />
      <Button
        className='message-input__btn'
        size='huge'
        labelPosition='right'
      >
        <Icon name='location arrow'/> Send
      </Button>
    </form>
  );
}

export default MessageInput;
