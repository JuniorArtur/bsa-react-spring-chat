import axios from "axios";
import api from "../../config/api";
import { call, put, takeEvery, all } from "redux-saga/effects";
import {
  FETCH_MESSAGE,
  FETCH_MESSAGE_SUCCESS,
  FETCH_MESSAGE_FAILURE
} from "./action-types";

export function* fetchMessage(action) {
  try {
    const message = yield call(
      axios.get,
      `${api.url}/messages/${action.payload.id}`
    );
    yield put({ type: FETCH_MESSAGE_SUCCESS, payload: { data: message.data } });
  } catch (error) {
    yield put({
      type: FETCH_MESSAGE_FAILURE,
      payload: { error: error.message }
    });
  }
}

function* watchFetchMessage() {
  yield takeEvery(FETCH_MESSAGE, fetchMessage);
}

export default function* messagePageSagas() {
  yield all([watchFetchMessage()]);
}
