import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

import "./message-editor.css";

import * as actions from "./message-editor-actions";
import { updateMessage } from "../../containers/Chat/chat-action";
import Spinner from "../Spinner";
import ErrorIndicator from "../ErrorIndicator";

const MessageEditor = ({
  token,
  history,
  match,
  // messages,
  // messageId,
  updateMessage,
  fetchMessage,
  messageData,
  loading,
  error
}) => {
  const [state, setState] = useState(messageData);
  const [message, setMessage] = useState("");

  useEffect(() => {
    if (match.params.id) {
      fetchMessage(match.params.id);
    }
  }, [fetchMessage, match.params.id]);

  useEffect(() => {
    setState(messageData);
    setMessage(messageData.message);
  }, [messageData]);

  const onCancel = () => {
    setMessage("");
    history.push("/");
  };

  const onSave = () => {
    updateMessage(messageData.id, { ...state, message });
    setMessage("");
    history.push("/");
  };

  if (!token) {
    history.push("/login");
  }

  if (loading) {
    return <Spinner />;
  }
  if (error) {
    return <ErrorIndicator error={error} />;
  }
  return (
    <section className="col-sm-9 col-md-7 col-lg-5 mx-auto">
      <div className="card card-signin my-5">
        <div className="card-body">
          <h5 className="card-title text-center">Edit Message</h5>
          <div className="form-label-group">
            <textarea
              className="form-control"
              rows="5"
              onChange={e => setMessage(e.target.value)}
              value={message}
            />
          </div>
          <div className="modal-footer">
            <button className="btn btn-secondary" onClick={onCancel}>
              Cancel
            </button>
            <button className="btn btn-warning" onClick={onSave}>
              Save
            </button>
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = state => {
  return {
    messageData: state.messageEditReducer.data,
    loading: state.messageEditReducer.loading,
    error: state.messageEditReducer.error,
    token: state.authReducer.data.token
  };
};

const mapDispatchToProps = {
  ...actions,
  updateMessage
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageEditor);
