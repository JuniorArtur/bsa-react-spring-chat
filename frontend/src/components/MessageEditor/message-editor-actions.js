import { FETCH_MESSAGE } from "./action-types";

export const fetchMessage = id => ({
  type: FETCH_MESSAGE,
  payload: {
    id
  }
});
