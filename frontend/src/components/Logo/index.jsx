import React from 'react';
import { Image } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
// import { connect } from 'react-redux';
// import { logout } from '../../containers/LoginPage/login-page-action';
//<Button basic icon type="button" onClick={logout}>
//         <Icon color="red" name="log out" size="big" />
//       </Button>
import './styles.css';

const Logo = ({image, title}) => (
  <div className='app__logo-container'>
    <div className='app__logo'>
      <Link to='/chat'><Image size='small' src={image} className='app__logo-img' alt='logo'/></Link>
      <h2 className='app__title'>{title}</h2>
    </div>
  </div>
);

export default Logo;
