import axios from "axios";
import api from "../../config/api";
import { call, put, takeEvery, all } from "redux-saga/effects";
import {
  FETCH_USER,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE
} from "./action-types";

export function* fetchUser(action) {
  try {
    const user = yield call(axios.get, `${api.url}/users/${action.payload.id}`);
    yield put({ type: FETCH_USER_SUCCESS, payload: { data: user.data } });
  } catch (error) {
    yield put({ type: FETCH_USER_FAILURE, payload: { error: error.message } });
  }
}

function* watchFetchUser() {
  yield takeEvery(FETCH_USER, fetchUser);
}

export default function* userPageSagas() {
  yield all([watchFetchUser()]);
}
