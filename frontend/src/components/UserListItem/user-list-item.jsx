import React from "react";

const UserListItem = ({id, name, surname, email, onEdit, onDelete}) => (
  <div className="container list-group-item">
    <div className="row">
      <div className="col-8">
          <span className="badge badge-outline-secondary d-block">
            {name} {surname}
          </span>
        <span className="badge badge-outline-danger d-block">{email}</span>
      </div>
      <div className="col-4 btn-group">
        <button className="btn btn-outline-dark" onClick={() => onEdit(id)}>
          Edit
        </button>
        <button
          className="btn btn-outline-danger"
          onClick={() => onDelete(id)}
        >
          Delete
        </button>
      </div>
    </div>
  </div>
);

export default UserListItem;
