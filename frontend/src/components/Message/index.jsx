import React, {useState} from 'react';
import {Dropdown, Feed, Icon} from 'semantic-ui-react';
import moment from 'moment';

import './styles.css';

const Message = ({message, author, likeMessage, editMessage, deleteMessage}) => {
    const [isHovering, setIsHovering] = useState(false);
    const {id, user, avatar, createdAt, text} = message;
    const date = moment(createdAt).fromNow();
    const messageOptionsDropdown = (
      <Dropdown inline icon='cog' className='ui right floated message__options'>
          <Dropdown.Menu>
              <Dropdown.Item onClick={() => {editMessage(id)}}>
                  <Icon name='edit'/> Edit
              </Dropdown.Item>
              <Dropdown.Item onClick={() => deleteMessage(id)}>
                  <Icon name='trash'/> Delete
              </Dropdown.Item>
          </Dropdown.Menu>
      </Dropdown>
    );
    return (
      <li
        onMouseEnter={() => setIsHovering(!isHovering)}
        onMouseLeave={() => setIsHovering(!isHovering)}
        className={'message message--' + (message.user === author ? 'out' : 'in')}
      >
          <Feed>
              <Feed.Event>
                  {avatar && (<Feed.Label><img src={avatar} alt='avatar'/></Feed.Label>)}
                  <Feed.Content>
                      <Feed.Summary>
                          <Feed.User> {user} </Feed.User>
                          <Feed.Date> {date} </Feed.Date>
                          {((user === author && isHovering) && messageOptionsDropdown)}
                      </Feed.Summary>
                      <Feed.Extra text> {text} </Feed.Extra>
                      <Feed.Meta>
                          <Feed.Like onClick={() => likeMessage(id)}>
                              <Icon name='like'/>
                          </Feed.Like>
                      </Feed.Meta>
                  </Feed.Content>
              </Feed.Event>
          </Feed>
      </li>
    );
};

export default Message;
