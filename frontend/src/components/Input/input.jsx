import React from "react";

const Input = ({ label, type, keyword, onChange, value }) => {
  return (
    <div className="form-label-group">
      <input
        type={type}
        id={label}
        className="form-control"
        value={value}
        onChange={e => onChange(e, keyword)}
        placeholder={label}
        required
        autoComplete="new-password"
      />
      <label htmlFor={label}>{label}</label>
    </div>
  );
};

export default Input;
