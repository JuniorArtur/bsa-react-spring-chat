import React from "react";

import "./error-indicator.css";

const ErrorIndicator = ({ error }) => {
  return (
    <div className="error-indicator">
      <span className="oops">Oops...</span>
      <span>Something has gone terribly wrong</span>
      <span>{error}</span>
    </div>
  );
};

export default ErrorIndicator;
