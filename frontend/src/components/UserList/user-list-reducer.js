import { FETCH_USERS } from "./action-types";

import { reduxHelper } from "../../helpers/reduxHelper";

const usersReducer = reduxHelper(FETCH_USERS, []);

export default usersReducer;
