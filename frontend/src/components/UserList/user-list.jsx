import React, { useEffect } from "react";
import { connect } from "react-redux";

import "./user-list.css";

import UserListItem from "../UserListItem";
import * as actions from "./user-list-actions";
import Spinner from "../Spinner";
import ErrorIndicator from "../ErrorIndicator";

const UserList = ({
  users,
  loading,
  error,
  history,
  fetchUsers,
  deleteUser,
  isAdmin,
  token
}) => {
  useEffect(() => {
    fetchUsers();
  }, [fetchUsers, token]);

  const onEdit = id => {
    history.push(`/user/${id}`);
  };

  const onDelete = id => {
    deleteUser(id);
  };

  const onAdd = () => {
    history.push("/user");
  };

  const onBack = () => {
    history.push("/");
  };

  if (!isAdmin) {
    history.push("/");
  }

  if (loading) {
    return <Spinner />;
  }
  if (error) {
    return <ErrorIndicator error={error} />;
  }

  return (
    <section className="col-sm-9 col-md-7 col-lg-5 mx-auto">
      <div className="card card-signin my-5">
        <div className="card-body">
          <h5 className="card-title text-center">Users list</h5>
          <div>
            {users.map(user => {
              return (
                <UserListItem
                  key={user.id}
                  id={user.id}
                  name={user.user}
                  email={user.email}
                  onEdit={onEdit}
                  onDelete={onDelete}
                />
              );
            })}
          </div>
          <button
            className="btn btn-outline-danger"
            onClick={onBack}
            style={{ margin: "5px" }}
          >
            Chat
          </button>
          <button
            className="btn btn-success float-right"
            onClick={onAdd}
            style={{ margin: "5px" }}
          >
            Add user
          </button>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = state => {
  return {
    users: state.usersReducer.data,
    loading: state.usersReducer.loading,
    error: state.usersReducer.error,
    token: state.authReducer.data.token,
    isAdmin: state.authReducer.data.isAdmin
  };
};

const mapDispatchToProps = {
  ...actions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList);
