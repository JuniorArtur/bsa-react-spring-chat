import React from 'react';
import Message from '../Message';
import {formatMessagesDivisorDate} from "../../services/dateTimeSerive";
import {sortMessagesPerDates} from "../../services/messageService";

import './styles.css';
import '../Message/styles.css';

const MessageList = ({messages, history, deleteMessage, toggleLike, currentUser}) => {
  const editMessageFunc = id => history.push(`/message/${id}`);
  const likeMessageFunc = id => toggleLike(id);
  const deleteMessageFunc = id => deleteMessage(id);

  document.onkeydown = function (e) {
    if (e.key === "ArrowUp") {
      const currentUserMessages = messages.filter(m => m.user === currentUser);
      const lastCurrentUserMessage = currentUserMessages[currentUserMessages.length - 1];
      if (lastCurrentUserMessage) {
        editMessageFunc(lastCurrentUserMessage.id);
      }
    }
  };
  const messagesByDates = sortMessagesPerDates(messages);
  return (
    <>
      {
        messagesByDates.map(dateMessages =>
          <>
            <div className='message-divider'>
              <span className='message-divider__line'/>
              <p className='message-divider__date'>{formatMessagesDivisorDate(dateMessages.date)}</p>
              <span className='message-divider__line'/>
            </div>
            {
              dateMessages.messages.map(message =>
                <ul className='message-list'>
                    <Message
                      key={message.id}
                      message={message}
                      author={currentUser}
                      likeMessage={likeMessageFunc}
                      editMessage={editMessageFunc}
                      deleteMessage={deleteMessageFunc}
                    />
                </ul>
              )
            }
          </>
        )
      }
    </>
  );
}

export default MessageList;
