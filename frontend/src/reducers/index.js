import { combineReducers } from "redux";
import chatReducer from "../containers/Chat/chat-reducers";
import messageEditReducer from "../components/MessageEditor/message-editor-reducer";
import usersReducer from "../components/UserList/user-list-reducer";
import userEditReducer from "../components/UserEditor/user-editor-reducer";
import authReducer from "../containers/LoginPage/login-page-reducer";

const rootReducer = combineReducers({
  chatReducer,
  messageEditReducer,
  usersReducer,
  userEditReducer,
  authReducer
});

export default rootReducer;
