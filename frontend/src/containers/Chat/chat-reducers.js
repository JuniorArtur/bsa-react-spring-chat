import { FETCH_MESSAGES } from "./action-types";

import { reduxHelper } from "../../helpers/reduxHelper";

const messagesReducer = reduxHelper(FETCH_MESSAGES, []);

export default messagesReducer;
