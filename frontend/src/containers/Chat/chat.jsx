import React, {useEffect} from "react";
import {connect} from "react-redux";
import {Menu} from "semantic-ui-react";
import * as actions from "./chat-action";
import Logo from "../../components/Logo";
import Header from "../../components/Header";
import MessageList from "../../components/MessageList";
import MessageInput from "../../components/MessageInput";
import Spinner from "../../components/Spinner";
import ErrorIndicator from "../../components/ErrorIndicator";

import logoImage from './chat-logo.svg';
import './styles.css';

const Chat = ({
                history,
                isAdmin,
                user,
                avatar,
                messages,
                users,
                deleteMessage,
                fetchMessages,
                toggleLike,
                addMessage,
                loading,
                error
              }) => {
  useEffect(() => {fetchMessages();}, [fetchMessages]);
  if (loading) {
    return <Spinner />;
  }
  if (error) {
    return <ErrorIndicator error={error} />;
  }
  history.push("/login");
  return (
    <div className='chat'>
      <Logo image={logoImage} title={'Chatify'}/>
      <Menu className='chat__header' borderless>
        <Menu.Item as='h1' header>
          <Header
            history={history}
            isAdmin={isAdmin}
            chatName={'Devs chat'}
            usersCount={users.length}
            messagesCount={messages.length}
            lastMessageDate={messages.length ? messages[messages.length - 1].created_at : null}
          />
        </Menu.Item>
      </Menu>
      <main className='chat__content'>
        <MessageList
          messages={messages}
          history={history}
          deleteMessage={deleteMessage}
          toggleLike={toggleLike}
          currentUser={user}
        />
      </main>
      <Menu className='chat__footer' borderless>
        <Menu.Item header>
          <MessageInput addMessage={addMessage} user={user} avatar={avatar} />
        </Menu.Item>
      </Menu>
      <footer className='chat__footer-title'>
        <p className='chat-footer__text'>&copy; BSA Team</p>
      </footer>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    messages: state.chatReducer.data,
    loading: state.chatReducer.loading,
    error: state.chatReducer.error,
    users: state.usersReducer.data,
    isAdmin: state.authReducer.data.isAdmin,
    avatar: state.authReducer.data.avatar,
    user: state.authReducer.data.user
  };
};

const mapDispatchToProps = {
  ...actions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
