import { SEND_LOGIN } from "./action-types";
import { reduxHelper } from "../../helpers/reduxHelper";

const initialState = {
  isAdmin: "",
  user: "",
  avatar: ""
};

const userReducer = reduxHelper(SEND_LOGIN, initialState);

export default userReducer;
