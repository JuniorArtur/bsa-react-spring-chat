import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import * as actions from "./login-page-action";
import Spinner from "../../components/Spinner";
import ErrorIndicator from "../../components/ErrorIndicator";
import Input from "../../components/Input";

import "./login-page.css";

export const LoginPage = ({ history, sendLogin, isAdmin, loading, error }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    if (isAdmin) {
      history.push("/users");
    }
  }, [history, isAdmin]);

  const onSubmit = e => {
    e.preventDefault();
    sendLogin(email, password);
    setEmail('');
    setPassword('');
  };

  if (loading) {
    return <Spinner />;
  }
  if (error) {
    return <ErrorIndicator error={error} />;
  }
  return (
    <section className="col-sm-9 col-md-7 col-lg-5 mx-auto">
      <div className="card card-signin my-5">
        <div className="card-body">
          <h1 className="card-title">Sign In</h1>
          <form className="form-signin" onSubmit={onSubmit}>
                <Input
                  label={'Email'}
                  type={'text'}
                  keyword={'email'}
                  onChange={e => setEmail(e.target.value)}
                  value={email}
                  key={'Email'}
                />
                <Input
                  label={'Password'}
                  type={'password'}
                  keyword={'password'}
                  onChange={e => setPassword(e.target.value)}
                  value={password}
                  key={'Password'}
                />
            <button
              className="btn btn-lg btn-outline-danger btn-block text-uppercase"
              type="submit"
            >
              Sign in
            </button>
          </form>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = state => {
  return {
    isAdmin: state.authReducer.data.isAdmin,
    loading: state.authReducer.loading,
    error: state.authReducer.error
  };
};

const mapDispatchToProps = {
  ...actions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
