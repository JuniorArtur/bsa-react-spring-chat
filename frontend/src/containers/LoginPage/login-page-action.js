import { SEND_LOGIN } from "./action-types";

export const sendLogin = (login, password) => ({
  type: SEND_LOGIN,
  payload: {
    login,
    password
  }
});
