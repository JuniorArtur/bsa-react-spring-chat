function reduxHelper(actionName, data) {
  const actionNameUpper = actionName.toUpperCase();
  const actionRequest = actionNameUpper;
  const actionSuccess = actionNameUpper + "_SUCCESS";
  const actionFailure = actionNameUpper + "_FAILURE";

  const initialState = {
    data: data,
    loading: false,
    error: null
  };

  return (state = initialState, action) => {
    switch (action.type) {
      case actionRequest:
        return {
          ...state,
          loading: true
        };

      case actionSuccess:
        return {
          ...state,
          loading: false,
          data: action.payload.data !== undefined ? action.payload.data : null
        };

      case actionFailure:
        return {
          ...state,
          loading: false,
          error: action.payload.error
        };

      default:
        return state;
    }
  };
}

export {reduxHelper};
