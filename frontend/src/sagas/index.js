import {all} from "redux-saga/effects";
import userPageSagas from "../components/UserEditor/sagas";
import messagePageSagas from "../components/MessageEditor/sagas";
import usersSagas from "../components/UserList/sagas";
import messagesSagas from "../containers/Chat/sagas";
import loginSagas from "../containers/LoginPage/sagas";

export default function* rootSaga() {
  yield all([
    userPageSagas(),
    usersSagas(),
    messagePageSagas(),
    messagesSagas(),
    loginSagas()
  ]);
}
